import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import operator

#global use
def make_it_global(path1,path2):
    global matches 
    global deliveries
    matches = list()
    deliveries = list()
    with open(path1) as m:
        reader = csv.DictReader(m)
        for line in reader:
            matches.append(line)
    with open(path2) as d:
        reader = csv.DictReader(d)
        for line in reader:
            deliveries.append(line)
make_it_global('ipl/matches.csv', 'ipl/deliveries.csv')

#--------------------------------------------------------------
# Part-1 Solution

def graph_no_of_match_played_per_year(dictionary_Obj):
    global sorted_keys 
    sorted_keys = sorted(dictionary_Obj)
    sorted_value = list()
    for i in sorted_keys:
        sorted_value.append(dictionary_Obj[i])
    plt.bar(sorted_keys, sorted_value,color='c')
    plt.xlabel('Year')
    plt.ylabel("Number's of matches")
    plt.title("Number\'s of match played per year")
    plt.show()

def no_of_match_played_per_year():
    save_in_dict = dict()
    for line in matches:
        if line['season'] in save_in_dict:
            save_in_dict[line['season']] = save_in_dict[line['season']] + 1
        else:
            save_in_dict[line['season']] = 1
    graph_no_of_match_played_per_year(save_in_dict)


#------------------------------------------------------------------------------------------------------------
# Part-2 Solution
def graph_matches_won_of_all_teams_over_all_the_years(result_in_dict):
    teams_name = []
    per_year_team_win = []
    result_in_dict_values = list()
    for key, value in result_in_dict.items():
        teams_name.append(key)
        result_in_dict_values.append(value)
    for index in result_in_dict_values:
        store_one_teams_matches = list()
        for year, win_per_year in index.items():
            store_one_teams_matches.append(win_per_year)
        per_year_team_win.append(store_one_teams_matches)
    bottom_point = [0]*10
    for win_count in per_year_team_win:
        team_lable = teams_name[per_year_team_win.index(win_count)]
        plt.bar(sorted_keys,win_count,width=0.5, bottom=bottom_point, label=team_lable)
        bottom_point = [x+y for (x,y) in zip(win_count, bottom_point)]
    plt.ylabel('No of Matches    ----------------->')
    plt.xlabel("Years    ----------------->")
    plt.title("Match won by team's in every season", y=-0.15)
    plt.legend(bbox_to_anchor=(1.05, 1),ncol=5, loc=4, borderaxespad=1)
    plt.show()


def matches_won_of_all_teams_over_all_the_years():
    save_in_dict = dict()
    for match in matches:
        if match['team1'] in save_in_dict:
            pass
        else:
            save_in_dict[match['team1']] = dict()
    for year in sorted_keys:
        for team in save_in_dict:
            count = 0
            for match in matches:
                if match['season'] == year:
                    if match['winner'] == team:
                        count += 1
            save_in_dict[team][year] = count
    graph_matches_won_of_all_teams_over_all_the_years(save_in_dict)

#----------------------------------------------------------------------------------------------
# Part-3 Solution

def graph_extra_run_conceded_per_team_in_2016(ploting_object):
    team_name = list()
    per_team_extra_runs = list()
    for teamName, extraRun in ploting_object.items():
        team_name.append(teamName)
        per_team_extra_runs.append(extraRun)
    plt.bar(team_name, per_team_extra_runs,width=0.7, color='c')
    plt.xlabel('Team name    -------------------->')
    plt.ylabel("Number's of extra runs    -------------------->")
    plt.title("Extra run conceded by per team in 2016 ")
    plt.show()



def extra_run_conceded_per_team_in_2016():
    run_conced_per_team = dict()
    for match_id in range(577,637):
        for line in deliveries:
            if line['match_id'] == str(match_id) and line['is_super_over'] == '0':
                if line['bowling_team'] in run_conced_per_team:
                    run_conced_per_team[line['bowling_team']] =int(run_conced_per_team[line['bowling_team']]) + int(line['extra_runs'])
                else:
                    run_conced_per_team[line['bowling_team']] = int(line['extra_runs'])
    
    graph_extra_run_conceded_per_team_in_2016(run_conced_per_team)

#------------------------------------------------------------------------------------------------------------------
# part-4 Solution

def graph_top_economical_bowlers_in_2015(plot_object):
    player_name = list()
    player_economy = list()
    for only_ten in range(0,10):
        player_name.append(plot_object[only_ten][0])
        player_economy.append(plot_object[only_ten][1])
    plt.bar(player_name, player_economy,width=0.7, color='c')
    plt.xlabel("Player's name   -------------------->")
    plt.ylabel("Economy    -------------------->")
    plt.title("Top 10 Bowler's in 2015")
    plt.show()


def top_economical_bowlers_in_2015():
    economical_bowlers = dict()
    economical_bowlers_dict = dict()
    for match_id in range(518,577):
        for line in deliveries:
            if line['match_id'] == str(match_id) and line['is_super_over'] == '0':
                if line['bowler'] in economical_bowlers:
                    if int(line['ball']) <= 6:
                        economical_bowlers[line['bowler']][0] = economical_bowlers[line['bowler']][0] + 1
                        economical_bowlers[line['bowler']][1] = economical_bowlers[line['bowler']][1] + int(line['total_runs'])
                    else:
                        economical_bowlers[line['bowler']][1] = economical_bowlers[line['bowler']][1] + int(line['total_runs'])
                else:
                    economical_bowlers[line['bowler']] = list()
                    economical_bowlers[line['bowler']].append(1)
                    economical_bowlers[line['bowler']].append(int(line['total_runs']))
       
    for key, value in economical_bowlers.items():
        economical_bowlers_dict[key] = (value[1]*6)/value[0]
    return_to_graph_economical_bowlers = sorted(economical_bowlers_dict.items(), key=operator.itemgetter(1))
    graph_top_economical_bowlers_in_2015(return_to_graph_economical_bowlers)

#-------------------------------------------------------------------------------------------------------------

# part-5 Solution

def some_others_facts():

    #first scenarios
    def graph_toss_winner_in_all_the_season(plot_object):
        team_names = list()
        no_of_wins = list()
        for key,value in plot_object.items():
            team_names.append(key)
            no_of_wins.append(value)
        plt.bar(team_names, no_of_wins,width=0.7, color='c')
        plt.xlabel("Teams name   -------------------->")
        plt.ylabel("Number's of win    -------------------->")
        plt.title("All team's win tosses throughout season")
        plt.show()
        print(team_names)
    def toss_winner_in_all_the_season():
        toss_winner_team = dict()
        for matche in matches:
            if matche['toss_winner'] in toss_winner_team:
                toss_winner_team[matche['toss_winner']] = toss_winner_team[matche['toss_winner']] + 1
            else:
                toss_winner_team[matche['toss_winner']] = 1
        graph_toss_winner_in_all_the_season(toss_winner_team)
        
    toss_winner_in_all_the_season()
def executeProblems():
    # no_of_match_played_per_year()
    # matches_won_of_all_teams_over_all_the_years()
    # extra_run_conceded_per_team_in_2016()
    # top_economical_bowlers_in_2015()
    some_others_facts()

executeProblems()